from tkinter import *
import csv
import sqlite3

class Car:

    def __init__(self,window):
        self.window = window
        self.window.title("Car Details")
        self.window.geometry("400x500")

        name = StringVar()
        type = StringVar()
        cost = StringVar()

        def clear():
                name.set("")
                type.set("")
                cost.set("")
                return

        def submit():
            item1 = name.get()
            item2 = type.get()
            item3 = cost.get()
            param = {
                   'NAME': item1,
                   'TYPE': item2,
                   'COST': item3
            }
            print(item1, item2, item3)
            car_data(param)
            return

        def car_data(data):
            conn = sqlite3.connect('Car_db.db')

            query = ('INSERT  OR REPLACE INTO CAR (CAR_NAME, CAR_TYPE, CAR_COST) '
                     'VALUES(:NAME, :TYPE, :COST)')

            conn.execute(query, data)
            conn.commit()
            conn.close()

        def show():
            display1 = Label(window, text=name.get()).place(x=35,y=390)
            display2 = Label(window, text=type.get()).place(x=35,y=410)
            display3 = Label(window, text=cost.get()).place(x=35,y=430)

        def report():
            with open('car_report.csv', mode='w+') as csv_file:
                writer = csv.writer(csv_file, delimiter=',')
                conn = sqlite3.connect('Car_db.db')
                query2 = ("SELECT * FROM CAR")
                details = conn.execute(query2)
                writer.writerow(['CAR_NAME', 'CAR_TYPE', 'CAR_COST'])

                line_count = 0
                for row in details:
                    if line_count == 0:
                        writer.writerow([row[0], row[1], row[2]])
                        line_count += 1

                    else:
                        writer.writerow([row[0], row[1], row[2]])
                        line_count += 1

                conn.commit()
                conn.close()

        self.label1 = Label(window, text="Car Name").place(x=20,y=90)
        self.entry1 = Entry(window, textvar=name).place(x=120, y=90, width= 130)

        self.label2 = Label(window, text="Car Type").place(x=20,y=150)
        self.entry2 = Entry(window, textvar=type).place(x=120, y=150, width=130)

        self.label3 = Label(window, text="Car Cost").place(x=20,y=210)
        self.entry3 = Entry(window, textvar=cost).place(x=120, y=210, width=130)

        self.button1 = Button(window, text="Submit", command=submit,font=("aerial", 10, "bold"))
        self.button1.place(x=40, y=280, width=70)

        self.button2 = Button(window, text="Show",command=show,font=("aerial", 10, "bold"))
        self.button2.place(x=120, y=280, width=70)

        self.button3 = Button(window, text="Clear",command=clear,font=("aerial", 10, "bold"))
        self.button3.place(x=200, y=280, width=70)

        self.button4 = Button(window, text="Report", command=report, font=("aerial", 10, "bold"))
        self.button4.place(x=120, y=350, width=70)

if __name__ == '__main__':
    window = Tk()
    app = Car(window)
    window.mainloop()